---
date:   2017-09-27 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

# Compte rendu du mercredi 27 septembre 2017


Pour Info: Les réunions se font à 12h45 tous les merecredis midis et club Info a un site: 
[Le site du club Info.](https://clubinfo.plil.net)


Au programme aujourd'hui:

- Présentation du premier jet de maquette pour le site du Bde =>Retardé: On voit ça la semaine prochaine
 
- Geoffrey va nous parler des serveurs que Club Info gère => Geoffrey absent

- Propositions de concours

- Club Info cherche une salle!

- Les tuto Benoit, les tuto Benoit! Aujourd'hui: Mais qu'est ce qu'un objet? (débutant)


A venir: 

- Un tuto Benoit: Mais qu'est ce qu'un objet? (expert): Dans 3 semaines

- Un tuto Geoffrey: La semaine prochaine

- Un tuto d'Hugo: Dans deux semaines

- Des tutos pour la nuit de l'info: Dans 3 semaines?

- Des discussions sur qui est enthousiaste pour le concours ENS Paris: La semaine prochaine

Il y a deux nouveaux! Encore! Il s'appellent Axel et Tracy. Soyez les bienvenus :D.
Et Club Info, soit gentil avec les nouveaux ;).

## Concours de programmation:

Language au choix, concours qui se passe à Paris, Acm-icpc swerk 2017.
Proposition d'un professeur d'informatique de l'ENS de Paris, qui propose un 
concours de programmation à Télecom Paris. On aura des équipes de 3, plus un coach 
éventuel. Les frais de participation sont de 120 euros par groupe, mais l'école peut 
éventuellement nous financer. Cependant, le trajet et 
Le concours se passe le 25 et 26 novembre.
On va vous mettre les détails sur Facebook.
Ha, et c'est un concours mondial.
Si on gagne, on nous offre un voyage d'une semaine aux Etats Unis.

## Salle du Club Info

Le Club Info recherche une salle, car il aimerait avoir une salle à lui, pour y 
mettre ses projets, son matos et y squatter tranquilles quand on veut. 
On est actuellement en train de le demander au Bde, et ils ont pas le temps de 
nous la donner, en espèrant que ça change.

## Le tuto Benoit

Voici le lien vers la source du tuto Benoit: [La programmation orientée objet.](https://squarebracketassociates.github.io/OOProgrammingMooc/)

Et on en profite pour fêter les 50 ans de la programmation orientée objet, aujourd'hui. Yahou.

En résumé: Un objet, c'est comme une porte ou un président, mais dans un programme. Les objets ont des attributs,
qui les caractèrisent, et des méthodes, qui définissent les interactions avec eux.

## La nuit de l'info

C'est génial, ne l'oubliez pas.

### Fin de la réunion! Une bonne journée/soirée/matinée à tous!
