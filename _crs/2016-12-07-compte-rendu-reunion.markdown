---
date:   2016-12-07 13:45:00 +0100
author: "Geoffrey Preud'homme"
tags: ci
---

# Bienvenue sur Facebook
Après avoir constaté que le Mattermost comme moyen de communication dans le club ne fonctionnait pas tellement, le bureau a décidé de créer un groupe Facebook.

# Bilan Nuit de l’Info
Tout le monde était motivé et a apprécié, c’était génial ! Au final on aurait pu faire un truc en HTML / PHP comme prévu, mais vu qu’on était parti sur du Java, on a continué ! On a gagné la troisième place du défi vidéo, merci Éloi !
Quelques trucs à améliorer pour l’année prochaine :

* Se préparer encore mieux !
* Utiliser un tunnel internet comme pour les 404. Ça évitera les nombreux problèmes qu’on a eu avec le proxy.

# On continue Kilobits (= le projet commencé pour la Nuit de l’Info) ?
Oui unanime 😄. Du coup on repartirait plus sur du HTML / PHP.

Les mini-projet « Rainbank » et « Jeu de la vie » sont mis de côté (probablement définitivement).  Pour le serveur mail Pulce, Nicolas et Geoffrey se trouveront un créneau pour faire ça.

# Nouveaux outils de gestion de projet
On va utiliser Trello pour gérer les projets à plusieurs. C’est tout bête, chaque cadre représente une tâche, et elles sont réparties par colonnes : « À faire », « En cours », « Terminé »… À consulter pour quand on veut travailler sur un projet.

Pour ceux qui ne l’ont pas fait, créez un compte sur Trello  (<https://trello.com/>) et envoyez un votre adresse mail qui a servi pour la création à un membre du bureau.

# Challenge machine learning
L’EESTEC (pour ceux qui ne savent pas, c’est une association visant à créer un réseau d’étudiants et d’entreprises en Europe) organise un challenge / hackaton sur le sujet du machine learning. D’après [Wikipédia](https://fr.wikipedia.org/wiki/Apprentissage_automatique), le machine learning c’est ça :

> L'apprentissage automatique ou apprentissage statistique (machine learning en anglais), champ d'étude de l'intelligence artificielle, concerne la conception, l'analyse, le développement et l'implémentation de méthodes permettant à une machine (au sens large) d'évoluer par un processus systématique, et ainsi de remplir des tâches difficiles ou impossibles à remplir par des moyens algorithmiques plus classiques.

La compétition sera composée de manches locales (donc pour nous à Lille) et d’une finale dans un autre EESTEC d’Europe. Des séminaires sur le sujet seront donnés entre novembre et décembre. Les manches locales se dérouleront entre janvier et mars et la finale en mai. Les équipes en compétition seront composées de 3 personnes.  Le premier (?) meetup BigData & Machine Learning aura lieu demain jeudi 8 décembre à 18h30 à Polytech (je sais pas quel amphi, mais ça devrait être marqué). Au programme :

* Optimisation de Yarn (Jean-Louis Quéguiner - CTO à Auchan Retail Data)
* Retour d'expérience sur Google Cloud pour le Big Data  (François Nguyen - Chief Data Officer à La Redoute)

Geoffrey y sera et prendra des notes.

# Photo de club
Ah vous êtes beau. Cyprien allongé comme d’hab’.  Florian qui se fait étrangler par Marianne pour la deuxième année consécutive. Beaucoup trop de claviers. Une personne qui ferme les yeux et c’est reparti pour la 14e prise. J’ai hâte de voir ce que ça donne 😁.
Mercredi prochain à 12h30 c’est la photo de l’associatif, c’est comme une photo de club, mais avec tous les clubs. Venez si vous pouvez, avec les couleurs du Club Info c’est même mieux (mais on vous en voudra pas si vous oubliez. L’année dernière on était pas représenté, donc si il y a au moins une personne qui vient, c’est déjà une amélioration !). On commencera la prochaine réunion plus tard de ce fait.

