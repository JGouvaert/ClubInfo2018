---
author: "Blas Simon"
tags: ci
date: 2018-02-14 13:45:00+02:00
---

Bonjour à tous! 

Voici mon dernier compte rendu pour le club Info :/ du 14 février 2018.

## Aujourd'hui, passations!! Et une armoire. Et du chocolat de Saint Valentin. Merci Justine :)

Egalement des discours, des larmes, et un résumé de la Geoffre.

### Aujourd'hui la décision:

Les votes ont eu lieu. Se sont présentés:

Au poste de président:
- Johnny
- Jutine

Au poste de trésorier: 
- Johnny
- Justine
- Baptiste

Au poste de secrétaitre: 
- Justine 
- Baptiste

Au poste de vice-président:
- Thomas

Au poste de respo com: 
- Taky

Johnny, après moult réflexions sera notre bien aimé...... "roulement de tambours!", président! :D

Sont élus: 
 - Johnny ou Justine, présidents à 7 voix contre 6
 - Baptiste secrétaire à 8 voix contre 3 blanc et 2 contre, snif,
 - Justine ou Johnny, trésoriers à 8 voix pour, 1 blanc et 4 hautes impédance,
 - Thomas vice-président, à 11,5 voix pour et 2 blanc
 - Taky respo com à 9,75 voix pour, 0.75 voix pour Thomas, 1 voix pour Justine et 0.5 blanc.

### La Geoffre

La geoffre est maintenant l'Acronyme officiel pour le club Info dans les écoles primaires. Hourra!

Le projet s'est bien passé. Des histoires en programmation ont été racontées, en deux fois une heure 1/4;
Les enfants ont été impressionnant, en comprenant très bien tout ce qui avait été expliqué, notamment avec la gestion des évènements.
Il y a quelques petites difficultés sur les boucles, mais c'était à attendre.

Une troisième scéance serait nécessaire pour expliquer notamment les boucles.
Plus de trois scéances semble pour le moment inutile, le but n'est pas de réaliser un cours
complet de programmation. 

Le scratch est maintenant au programme dans les écoles primaires, et notre intervention
permet à la fois d'apprendre aux élèves et au professeurs comment programmer.

La chose est motivante pour les enfants, il peuvent faire parler leur imagination,
montrer à leurs parents ce qu'ils ont fait, leurs oeuvres sont stockées et ils peuvent les
ressortir pour utilisation future.

L'objectif à long terme est qu'une fois que l'intervention sera faite dans de multiples écoles, 
organiser une rencontre pour que tout le monde puisse montrer ce qu'ils ont fait, 
sans notion de concours ou compétition, simplement profiter des différentes réalisations.

## 13h46, fin de la réunion

### Une bonne journée à tous

Snif




