---
date:   2017-10-11 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

### Bonjour à toutes et à tous!

On a un nouveau!! (oui, encore) Il s'appelle Johnny!

## Au programme aujourd'hui

- La nuit de l'info

- Redirection Email pulse

- Premier apercu site web BDE

- Venez sur Gitlab!!

- Les tutos d'Hugo!

## Au programme plus tard

- La Nuit de l'Info (encore et toujours)
- Des tutos, toujours plus de tutos

## Emails Pulce

Ils vont nous faire une demande pour rediriger toutes les emails vers une nouvelle adresse en polytech-lille.fr.
On doit donc rediriger toutes leurs adresses vers leur mail Polytech.

## Gitlab

Que tout le monde se crée un compte et contacte le préz pour lui donner son identifiant:

- Aller sur gitlab.io

- Cliquer sur "use gitlab"

- Se créer un compte ou se connecter

- Donner son pseudo au président (aka Benoît) pour qu'il vous ajoute au groupe

Bravo, vous êtes désormais à peu près officiellement au club Info!

## La nuit de l'Info

#### Les volontaires:
- Baptiste
- Benoit
- Hugo
- Nicolas
- Johnny
- Robin
- Maeva
- Simon

#### Demander à:
- Eloi?
- Geoffrey?
- Justine?

Si d'autres personnes qu'on a pu oublier veulent participer à la nuit de l'info, 
n'hésitez pas à le dire aux responsables de la nuit de l'Info ;), à savoir Simon Blas et Maëva Delaporte.

#### Les suggestions:
- Proposer à des gens de nous défier, pour créer d'autres équipes dans Polytech
- Voir si une équipe 404 va se former, qui va passer sa nuit à jouer
- Les tutos "nuit de l'info" vont arriver à partir de la semaine prochaine avec
un tuto git


## Le site du Bde

Ca avance, c'est sympa. Benoit et Nicolas y travaillent ensembles, et ont pour
l'instant réussi à faire un site très joli, mais pas encore fonctionnel.
Le site du Bde est sur le Gitlab du club Info, dans le dossier des clubs.

#### Propositions
- ajouter un carroussel d'images au milieu de la page d'accueil
- ajouter un petit mot en haut de la page d'accueil

## Les tutos d'Hugo!

Aujourd'hui, un tuto sur le bios. Retrouvez-le plus tard sur le [Gitlab](https://gitlab.com/ClubInfoPolytechLille/clubinfopolytechlille.gitlab.io/tree/master/_tutos) 
ou [Le site](https://clubinfopolytechlille.gitlab.io/tutos.html) du club Info.
N'oubliez pas que tous les tutos fait précedemment sont disponibles à ces liens! ;)

Voila voila, fin de la réunion. 

### Une bonne journée à tous!!