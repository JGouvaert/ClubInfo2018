---
date:   2016-12-14 13:45:00 +0100
author: "Geoffrey Preud'homme"
tags: ci
---

# Flashmob de l'associatif

Le principe aurait été de faire une chorée club
de 30 secondes et une chorée avec tous les autres clubs d'une
minute. Les répétitions sont pendant les examens donc personne
n'est chaud pour faire la chorégraphie, pas même les autres clubs
:)

# Kilobits

Le repo est prêt pour bosser.

On utilise le protocole REST pour accéder et
modifier les ressources depuis les applications clientes et le
serveur. Pour cela, c'est Spring qu'on utilise (à titre informatif,
on utilise plus le faux Spring mais le vrai. Ça a rien changé parce
qu'on l'avait pas utilisé ^^).

On peut demander à ce qu'une ressource soit
récupérée (GET), ajoutée (POST), modifiée (PUT) ou supprimée
(DELETE), que l'on envoie à un certaine adresse correspondant à la
ressource.

Exemple de déclaration d'une classe, que
l'on crée pour chaque type de ressource.

```java
@RestController
@RequestMapping("/user")
public class UserRest {
```

Exemple d'une fonction :

```java
@RequestMapping(value = "/list", method = RequestMethod.GET)
public List<User> getAllUser() {
	return dao.getAllUser();
}
```

Par exemple, ici en faisant une
requête GET sur `/user/list` le serveur nous retournera la liste des
utilisateurs.

Plus d'exemples :
[https://github.com/ClubInfoPolytechLille/kilobits-serv/blob/master/src/main/java/com/tbe/rest/UserREST.java](https://github.com/ClubInfoPolytechLille/kilobits-serv/blob/master/src/main/java/com/tbe/rest/UserREST.java)

On utilise JDBI pour simplifier la
manière dont on accède aux bases de données. On peut du coup
écrire une requête en deux lignes (à peu près).

On crée aussi une classe par type de
ressource :

```java
public interface UserDao {
	String strCreateUtilisateurTable = "Create table if not exists Utilisateur (...)"
```

Et on déclare ici ce qui est était
appelé dans la fonction REST pour récupérer les utilisateurs :

```java
@SqlQuery("Select * from utilisateur")
@RegisterMapperFactory(BeanMapperFactory.class)
List<User> getAllUser();
```

Un autre exemple avec modification de données
(donc POST) :

```java
@SqlUpdate("insert into langue (langue) values (:langue)")
@GetGeneratedKeys
int addLangue(@Bind("langue") String langue);
```

Et tout ça, ça se passe ici :
[https://github.com/ClubInfoPolytechLille/kilobits-serv/tree/master/src/main/java/com/tbe/database](https://github.com/ClubInfoPolytechLille/kilobits-serv/tree/master/src/main/java/com/tbe/database)

Petites explications sur le code
directement, c'est un peu compliqué à écrire directement sur un
compte rendu donc fermez les yeux et imaginez. Ou ouvrez le code
c'est mieux.

Ré-éxplication de Trello, cf [compte-rendu de la
dernière fois](https://clubinfo.plil.net/cr/2016/12/07/compte-rendu-reunion.html#nouveaux-outils-de-gestion-de-projet).
