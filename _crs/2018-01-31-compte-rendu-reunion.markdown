---
date:   2018-01-31 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

Bonjour à tous! Voici le compte rendu de la réunion du 31 janvier.

### C'est bienôt la chandeleur!!! :D

## Le point passations

Elles doivent se faire soit la semaine prochaine, soit la semaine d'après. Dépéchons nous, ça va être un peu compliqué.

## Le point école

Trois enfants dormaient. d'autres ont trouvé leur voie vers l'informatique.
Nicolas et Hugo ont émerveillé des enfants, qui ont eu l'air d'apprécier l'informatique.
Les explications se sont bien passées.

Le projet pour la prochaine scéance est de raconter une histoire en utilisant scratch.
La deuxième scéance a été planifiée à l'oral, et tout semble bien se passer.

La maitresse était très satisfaite dans l'ensemble, Hugo et Nicolas y retourneront dans deux semaines.

Hugo a réussi à répondre à la question difficile: Qu'es-ce qu'un bug?

Petite anecdote historique: A la belle époque, des ordinateurs très volumineux, un bug était un insecte qui entrait dans la pièce et mangeait les câbles.

## Le point projet école

Pour l'instant, on a pas assez de gens pour réaliser ce projet école. Peut-être l'année prochaine.

Benoit est différent de Geoffrey. Benoit à les cheuveux moins longs.


### Fin de la réunion