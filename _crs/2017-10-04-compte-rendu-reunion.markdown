---
date:   2017-10-04 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

### Bonjour à toutes et à tous!

Il est midi 49 et 40 secondes.

  
## Au programme aujourd'hui:

- on a migré sur gitlab

- le concours acm-icpc swerc

- le club info dans des écoles

- tuto geoffrey

- la nuit de l'info


## Au programme plus tard:

- tuto d'Hugo!

- plus de news sur la nuit de l'Info

- tutos nuit de l'info
  
  
### La grande migration

On est passés sur gitlab. Vous savez ce que c'est? Tant mieux. Le site s'appele gitlab.io,
c'est la même chose que le github, donc inscrivez-vous y! Et faites vous ensuite
inviter sur le gitlab du club info.

### Le concours

Il se déroule les 25 et 26 Novembre. On va avoir un équipe formée de Geoffrey, Baptiste et Benoit, qu'on enverra là-bas.
Bonne chance à eux!

### Le club info dans les écoles  (ce n'est pas la CREP!)

Depuis quelques années, l'enseignement de la prog est obligatoire en école primaire.
Mais les profs ont pas toujours les moyens ou la formation pour le faire, et le club
Info propose de palier à ce manque, sans faire concurrence à la CREP bien évidemment.
On cherche des volontaires ;) .On va demander l'accord de Polytech et de l'éducation 
nationale, mais on a des contacts, et de bonnes bases.

### Les tutos Geoffrey 

L'administration système:
Allez voir sur Gitlab, il y est/sera bientôt.

### La nuit de l'info

On a la confirmation de Redon qu'on aura accès à des switchs, un serveur et des câbles.
On va commencer à former les équipes maintenant, donc si vous êtes intéressés, manifestez-vous.
  
  

## Fin de la réunion! 

Une bonne journée à tous!