---
date:   2018-02-07 13:45:00 +0100
author: "Simon Blas"
tags: ci
---


Bonjour à tous! Voici le compte rendu du 7 février 2018.


### La semaine prochaine, passations!! 

Ce sera l'occasion de transmettre la clé du club Info et la caisse du club Info!
On pourra faire un joli discours, Benoit pourra dire que c'était mieux avant, etc...
On va essayer de trouver des volontaires.

A noter: sur la clé du club Info se situe un dossier caché dont on a pas le mot de passe.
Futur projet du club Info, cracker sa propre clé?

Les passations du Bde se feront début Mars.

Sont disponibles pour le bureau de l'année prochaine:
- Justine
- Johnny
- Baptiste
- Hugo?
- Nicolas?
- Xavier! Vice-président de sa propre volonté :).

Si vous voulez vous porter volontaires pour le bureau, surtout sachez que cotiser n'est pas nécessaire.

Pour la semaine prochaine, que tout le monde (Johnny) se choisisse un poste! :) 


### Demain, que fait-on?

Il semblerait que le club Info ait envie de crèpes.
Et accessoirement, le mini projet sera fait dans l'école test de la Geoffre.
Les élèves vont réaliser un petit programme en scratch.


### Pour la semaine prochaine

- Un tuto scratch de Justine!!
- Une armoire pour le club Info!
- Une photo avec Johnny président
- Marianne qui participera aux élections
- Un discours du président
- Un tuto

Voila voila, fin de la réunion, une bonne journée à tous!

