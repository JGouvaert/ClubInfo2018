---
date:   2017-05-17 13:45:00 +0100
author: "Geoffrey Preud'homme & Simon Blas"
tags: ci
---

**LE SECRÉTAIRE EST EN RETARD, L'ANCIEN BUREAU A RÉALISÉ UN COUP D'ÉTAT, C'EST MOI QUI ÉCRIT, C'EST MOI QUI AIT LE POUVOIR !!!**

Désolé...

# La boutique BDE

Le BDE aimerait nous faire faire réaliser une boutique en ligne pour que les étudiants puissent acheter des goodies...

Il faut qu'on fasse le cahier des charges avec le BDE, il faut qu'on les contacte.

# À propos du local

Il était question d'avoir un local à partager avec Robotech...

--Merci Geoffrey, je reprend l'antenne--

Il y a eu une réunion hier, qui portait sur les changements de salle de Robotech, l'Estec et nous.

L'Eestec se fait virer de sa salle pour un pôle langue, car apparemment, le directeur ignorait leur existence.

Les armoires de Robotech seront déplacées au Foyer, la grande salle au sous-sol où 
les gens dansent, qui accepte gentillement de les stocker temporairement.

Pour la nouvelle salle du club info, le vice-directeur propose donc de regrouper club Info, Robotech et L'Eestec, dans une salle pédagogique réaménagée.
La date reste inconnue, mais on est certains que ça sera avant le 12 juin.

Postez dans les commentaires si ça vous dérange ou pas!

# L'ordinateur du Bds a été réparé!

Merci à Nicolas!

# Wanna Cry: un virus!!!

Profil:
Il a figé une usine Renauld et Fedex aux Etats-Unis, et est très dangereux, surtout pour les entreprises.

Mode opératoire:
Il chiffre les données d'un ordinateur, et demande une rançon (en Bit-Coins) pour enlever le chiffrement de celui-ci. Un virus très très méchant :/  

Histoire:
Un groupe de hackeurs a hacké la Nsa, et a récupèré un fichier contenant une faille de Windows,
ce qui est assez inquiètant. 
Un patch de sécurité à été fait le 14 mars ce qui est déjà un bon début,
mais tous les Windows pas à jour sont toujours sous risque. Les risques sont faibles si vous avez un bon
fire-wall, mais c'est dangereux pour les entreprises, plus vulnérables.

Héros:
Un anglais a cependant trouvé une parade, et a réservé le nom de domaine qu'utilisait le virus pour fonctionner,
ce qui a très fortement ralenti la propagation du virus. Il a reçu une récompense de 10 000 euros, qu'il a aussitôt 
donné à la charité. Il a cependant conservé une pizza. Bravo et merci à lui!




Fin de la réunion! Une bonne journée à tous.