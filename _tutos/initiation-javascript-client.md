---
title: Initiation au Javascipt client
tags: js
author: "Geoffrey Preud'homme"
date: 2015-12-02 21:13:29+01:00
---

# Initiation au Javascript client

## Mise en place

Nous prendrons ce code HTML de base

```html
<!DOCTYPE html>
<html>
        <head>
                <title>Le Club Info c'est cool !</title>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
                <script type="text/javascript">
                    alert('Hello !');
                </script>
        </head>
        <body>
                <h1>Yo !</h1>
                <p class="ci">J'ai la classe du <strong>CI</strong> !</p>
                <p id="carambar">Pas moi, mais au moins je suis <strong>unique</strong> grâce à mon id !</p>
                <p>Moi aussi j'ai la classe du <strong>CI</strong>, qu'est-ce que vous allez tous faire ?</p>
        </body>
</html>
````

Le code du Javascript se situe dans la balise `<script>`. Vous pouvez aussi utiliser un outil très puissant de votre navigateur, la console, avec <kbd>Ctrl</kbd>+<kbd>Maj</kbd>+<kbd>J</kbd>. Cela vous permet d'entrer du code Javascript et de le voir s'éxecuter instantanément. Il ne sera cependant pas sauvegardé, ce n'est donc utile que pour du debug (car les messages d'erreurs y apparaissent) ou tester le nom des fonctions que vous allez utiliser avant de les <del>écrire au tableau</del> taper dans votre fichier.

## Bases

Javascript est assez similaire au C++, voire au Java. La différence princiaple situe dans le fait que les variables n'ont pas un type assigné et peuvent en changer à la volée.

Voici quelques exemples usuels :
```js
// Un commentaire
/* Un
bloc
de
commentaire */
var maVar = 'Le contenu, ici une chaîne de caractères. C\\'est cool non ?'; // Attention les variables sont définies localement, ça veut dire que si vous créez une variable dans une fonction, elle ne sera accessible uniquement à l'interieur de cette fonction.
var liste = ['fraises', 'bananes', 'merci', 42, 'la réponse D'];
var dictionnaire = {prez : 'Geoffrey', viceprez : 'Kévin', tresorier : 'Jean', secretaire : 'Pierre'};
var leSeulGisDuBureau = "VICEPREZ".toLowerCase();
console.log(liste[2], dictionnaire.tresorier, dictionnaire[leSeulGisDuBureau]) // affiche "merci", "Jean", "Kévin"
function tartiflette(patates, lardons, fromage) {
    return patates + lardons - fromage^2;
}
var couleur = prompt("Quelle est ta couleur préférée ?", "vert")
```

## Manipuler le HTML, version officielle
(en vrai on s'en fiche mais comme je suis un puriste je peux pas me passer de vous faire une petite introduction à cette méthode officielle.)

Javascript propose le Document Object Model (ou DOM), qui est un moyen très puissant de manipuler le code HTML de votre page. Par exemple :
```js
document.getElementsByTagName('h1')[0].innerText = 'Tadaaaa !';
```

## Des bibliothèques
JavaScript possède un nombre incalculable de bibliothèque permettant de se simplifier pas mal la tâche, donc pourquoi ne pas les utiliser pour éviter d'écrire un code imbitable comme celui ci-dessus ?

On va essayer avec [jQuery](http://jquery.com/), qui est probablement la plus populaire. Pour la charger dans votre code, rajoutez ceci à la balise `<head>` :
```js
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
```

Du coup vous avez une rimbabelle de fonctions qui s'offrent à vous.
```js
$('h1').innerText('Tadaaaa !'); 
$('.ci').css('background', 'pink'); // Pour référence, ce qui est dans $(...) s'appelle un sélecteur
$('p').click(function clicSurP() { // Pour référence on appelle ça un évènement (clic, souris qui passe, touche du clavier appuyé...)
    $(this).text('Hihi, ça chatouille !'); // Ici this est une variable spéciale qui contient le contexte, ici l'élément cliqué
}
$('#carambar strong').animate({fontSize: "50px"});
$('body').append($('<p>').text('Je viens de naître !')); // $('<el>') crée l'élément el, ce n'est pas vraiment un sélecteur.
```

Et j'en passe et des meilleurs. Vous pouvez retrouver toute la doc de jQuery [ici](http://api.jquery.com/). Bien sûr, c'est un peu la décharge, mais faire chercher `jquery savoir si un élément possède une classe` ou `jquery selecteur élement fils unique`sur Google fonctionne très bien (voire même mieux en anglais, puisqu'il y a le site StackOverflow pour ce genre de question et qu'en français vous tomberez sur des forums la plupart du temps).

## Bonus
```js
$.getScript('http://www.cornify.com/js/cornify.js', function(){
  cornify_add();
  $(document).keydown(cornify_add);
});
```
