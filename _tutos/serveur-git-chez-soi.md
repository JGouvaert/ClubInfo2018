---
title: "Se connecter au serveur git du CI depuis chez soi"
tags: ci git archive
author: "Geoffrey Preud'homme"
date: 2014-11-22 17:43
---


J'allais pour la n-ième fois chercher sur internet la commande exacte pour me connecter au serveur git (qui se pronnonce Git et non Jit ^^ [[source]](http://youtu.be/4XpnKHJAok8?t=1m30s)) du Club Info quand je me suis dit : "Faudrait quand même que je la note quelque part cette commande ...". Et puis je me suis dit que vu qu'on a désormais un groupe Google (houra !) j'allais la partager avec vous, parce que c'est quand même bien pratique.

Un peu de théorie :

On peut se connecter au serveur du club info de trois manières différentes :

* Depuis une des machines de Polytech
* Depuis le Wi-Fi PolytechLille
* Depuis le l'accès SSH exterieur de Polytech (portier.polytech-lille.fr:2222)

Comme aucune des deux premières méthodes  ne nous convient, nous allons utiliser la troisième.

Pour tester, essayez ceci dans un terminal :
```bash
ssh [identifiant polytech]@portier.polytech-lille.fr -p 2222
```

Tapez votre mot de passe Polytech, vous voilà connecté au portier.

Puis depuis le portier, pour le serveur du club info :

```bash
ssh git@servclubinfo.insecure.deule.net
```

(si vous avez la variable $CI, rien ne vous empêche de l'utiliser)

Là, vu que le serveur du Club Info fonctionne par un système de clef et non par mot de passe, il y a deux possibilités :

* Soit votre clef publique est dans votre répertoire personnel sur les ordis de Polytech (~/.ssh/id_rsa), ce qui est le cas si vous vous êtes déjà connecté au serveur du Club Info depuis un des ordis de Polytech avec votre session. Vous aurez alors accès à la liste des dépôts git que vous pouvez accéder.
* Soit vous ne vous êtes connecté que depuis votre ordinateur personnel, dans ce cas il vous demandera votre mot de passe, même si aucun mot de passe ne mènera à rien.
* Soit vous n'arrivez pas à vous connecter pour une raison X ou Y, la suite risque alors de ne pas fonctionner

Le problème, c'est que pour utiliser git, on ne peut spécifier directement qu'un seul SSH, et pas une composition de plusieurs. On va utiliser alors une technique qui s'appelle le port forwarding. 
Sans trop entrer dans les détails, cette technique va créer un port virtuel sur votre machine qui va être relié directement au serveur du Club Info par l'intermédiaire du portier.

Pour cela, ouvrez un nouveau terminal, et tapez :
```bash
ssh -L 1234:servclubinfo.insecure.deule.net:22 [identifiant polytech]@portier.polytech-lille.fr -p 2222
```

1234 étant un port de libre sur votre machine.

Tapez votre mot de passe Polytech, et vous devrez atterrir sur le portier. Cependant, le port vers le serveur est ouvert, et le sera jusqu'à ce que vous fermiez ce terminal. Laissez-le donc ouvert ! À chaque fois que vous voulez vous connecter au serveur du Club Info depuis l’extérieur, il faudra taper cette commande.

Vous pouvez tester la connexion avec

```
ssh git@localhost -p 1234
```

Normalement, la connexion devrait se faire sans encombre, car c'est votre machine qui est utilisée pour l'authentification, et devrait donc utiliser la clef qui se trouve à l'emplacement `~/.ssh/id_rsa`. Si vous avez une fait une configuration custom de ssh (`~/.ssh/config`), vous devriez savoir le modifier (pour ma part, j'utilise [ça](http://pastebin.com/spyMQkrN)). Si ça ne marche pas, ce qui suit ne fonctionnera pas non plus.


Puis, déplacez vous dans un répertoire git que vous avez auparavant cloné (si vous n'en avez pas, passez au paragraphe suivant, mais n'oubliez pas de lire celui-ci après). Nous allons ajouter une "remote" au serveur git qui va lui permettre de garder en mémoire le lien vers le serveur. Vous pouvez l'appeler comme vous voulez, moi je penche pour "origine", avec "origin" parce que c'est le nom par défaut pour une "remote" (et la "remote" appelée "origin" de votre répertoire devrait normalement pointer vers le club info directement, et ne fonctionne donc pas depuis chez vous), et "e" pour extérieur.

Tapez donc :

```bash
git remote add origine ssh://git@localhost:1234/polydex_kernel.git
```

Voilà, maintenant, vous devriez pouvoir faire un

```bash
git pull origine master
```

pour pouvoir récupérer tous les changements depuis le serveur. De la même manière, vous devriez pouvoir faire un

```bash
git push origine master
```

pour mettre vos changements sur le serveur.

N'oubliez pas cependant que à Polytech, il faudra continuer à utiliser "git push origin master" (ou "git push" tout court, ce qui revient -normalement- au même) (de même avec "pull")

Et si jamais on veut ajouter un dépôt qu'on a pas encore cloné ? Très simple :
```
git clone ssh://git@localhost:1234/polydex_story.git
```
Par contre, la remote "origin" sera lié à l'accès au serveur depuis l'exterieur de Polytech, il faut donc rajouter la remote "origine" puis remplacer la remote "origin" par l'accès depuis l’intérieur de Polytech pour garder une cohérence avec les dépôts déjà clonés. Tapez donc :

```
cd polydex_story
git remote add origine ssh://git@localhost:1234/polydex_story.git
git remote remove origin
git remote add origin ssh://git@servclubinfo.insecure.deule.net/polydex_story.git
```

Et normalement, tout est bon !


N'hésitez pas à mettre un message si ça a marché ou si ça n'a pas marché en indiquant le contenu de l'erreur et l'étape à laquelle vous êtes ;-)

